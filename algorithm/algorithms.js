var Constant = require('../tool/constants');
var log = require('../tool/consoleLogs');


var startIndexOfGap = -1;
var detectFirstNoiseZone = function (currentNoiseArrayIndex, arrData, result) {


    var numberOfValueAboveNoiseMax = 0;
    // log.log("currentNoiseArrayIndex : " + currentNoiseArrayIndex, 's');
    for (var i = currentNoiseArrayIndex; i < arrData.length - 1; i++) {
        if (arrData[i] !== undefined && arrData[i].noiseValue >= Constant.WAKE_UP_VALUE) {
            numberOfValueAboveNoiseMax++;
        } else
            numberOfValueAboveNoiseMax = 0;
        if (arrData[i] !== undefined && numberOfValueAboveNoiseMax === Constant.NBR_OF_RESULT) {
            while (i < arrData.length && arrData[i].noiseValue >= Constant.WAKE_UP_VALUE)
                i++;
            startIndexOfGap = i++;
            detectSecondNoiseZone(i + 1, arrData, function (endIndexOfGap) {
                result(startIndexOfGap, endIndexOfGap);
                return;
            });
        }
    }
    result(currentNoiseArrayIndex + 1, -1);

    //
    // var numberOfValueAboveNoiseMax = 0;
    // var nextNumberOfValueAboveNoiseMax = 0;
    // // log.log("currentNoiseArrayIndex : " + currentNoiseArrayIndex, 's');
    // for (var i = currentNoiseArrayIndex; i < arrData.length - 1; i++) {
    //     if (arrData[i] !== undefined && arrData[i].noiseValue >= Constant.WAKE_UP_VALUE) {
    //         numberOfValueAboveNoiseMax++;
    //     } else
    //         numberOfValueAboveNoiseMax = 0;
    //
    //
    //     if (arrData[i] !== undefined && numberOfValueAboveNoiseMax === Constant.NBR_OF_RESULT) {
    //         // while (i < arrData.length && arrData[i].noiseValue >= Constant.WAKE_UP_VALUE)
    //         //     i++;
    //         // for (var a = i + 1; a < i + 30; a++) {
    //         //     if (arrData[a] && arrData[a].noiseValue >= Constant.WAKE_UP_VALUE) {
    //         //         for (var x = a; arrData[x] && arrData[x].noiseValue >= Constant.WAKE_UP_VALUE; x++) {
    //         //             nextNumberOfValueAboveNoiseMax++
    //         //         }
    //         //         if (nextNumberOfValueAboveNoiseMax === 2) {
    //         //             // numberOfValueAboveNoiseMax = 0;
    //         //             // nextNumberOfValueAboveNoiseMax = 0;
    //         //             a = i + 30
    //         //         }
    //         //     }
    //         // }
    //         //
    //         // if (nextNumberOfValueAboveNoiseMax === 2) {
    //         //     startIndexOfGap = x + i;
    //         // } else {
    //         //     startIndexOfGap = i++;
    //         // }
    //         // nextNumberOfValueAboveNoiseMax = 0;
    //         // numberOfValueAboveNoiseMax = 0;
    //         detectSecondNoiseZone(startIndexOfGap + 1, arrData, function (endIndexOfGap) {
    //             result(startIndexOfGap, endIndexOfGap);
    //             return;
    //         });
    //     }
    // }
    // result(currentNoiseArrayIndex + 1, -1);
};

var detectSecondNoiseZone = function (currentEndFirstNoiseZoneIndex, arrData, result) {
    var numberOfValueAboveNoiseMax = 0;
    var endIndexOfGap = -1;


    for (var i = currentEndFirstNoiseZoneIndex; i < arrData.length - 1; i++) {
        if (Constant.USE_LIGHT_DATA) {
            if (arrData[i] !== undefined && arrData[i].noiseValue >= Constant.WAKE_UP_VALUE && arrData[i].lightValue > Constant.DEFAULT_LIGHT_VALUE) {
                numberOfValueAboveNoiseMax++;
            } else
                numberOfValueAboveNoiseMax = 0;
        } else {
            if (arrData[i] !== undefined && arrData[i].noiseValue >= Constant.WAKE_UP_VALUE) {
                numberOfValueAboveNoiseMax++;
            } else
                numberOfValueAboveNoiseMax = 0;
        }
        if (arrData[i] !== undefined && numberOfValueAboveNoiseMax === Constant.NBR_OF_RESULT) {
            endIndexOfGap = i - Constant.NBR_OF_RESULT;
            if (endIndexOfGap >= arrData.length)
                endIndexOfGap = -1;
            result(endIndexOfGap);
            return;
        }
    }
    if (i >= arrData.length)
        i = -1;
    result(i)
};


var getNumberOfNoisePik = function (arrData, details) {
    var arrNoisPiks = [];

    for (var i = details.startIndex; i < details.endIndex; i++) {
        if (arrData[i].noiseValue > Constant.WAKE_UP_VALUE) {
            arrNoisPiks.push({
                minuteIndex: i,
                value: arrData[i].noiseValue
            });
        }
    }
    // console.log("getNumberOfNoisePik: startIndex : " + details.startIndex + " endIndex : " + details.endIndex);
    return (arrNoisPiks)
};

module.exports = {

    getStartEndSleep: function (arrData, result) {
        var det = {};
        var dif = 0;
        startIndexOfGap = -1;

        for (var i = 0; i < arrData.length - 1; i++) {
            let va = ((arrData[i].minuteIndex / 30) + 12).toFixed(2);
            if (va > 24)
                va -= 24;
            // if (Constant.USE_LIGHT_DATA) {
            //     if (arrData[i].noiseValue >= Constant.WAKE_UP_VALUE)
            //         log.log("i : " + i + JSON.stringify(arrData[i]) + " time : " + va + " h / light : " + arrData[i].lightValue + " constante noise = " + Constant.WAKE_UP_VALUE, 'i');
            //     else
            //         console.log(JSON.stringify(arrData[i]) + " time : " + va + " h")
            // } else {
            // if (arrData[i].noiseValue >= Constant.WAKE_UP_VALUE)
            //     console.log('\x1b[31m%s\x1b[0m', "i : " + i + JSON.stringify(arrData[i]) + " time : " + va + " h");
            // else
            //     console.log(JSON.stringify(arrData[i]) + " time : " + va + " h")
            // }

        }
        for (let i = 0; i < arrData.length; i++) {
            detectFirstNoiseZone(i, arrData, function (startGapIndex, endGapIndex) {
                if (endGapIndex === -1) {
                    i = arrData.length + 1;
                    return;
                }
                // log.log("dif gap: " + (endGapIndex - startGapIndex) + " / old dif : " + dif, 'b');
                if (endGapIndex - startGapIndex > dif) {
                    // log.log("result startIndexOfGap isInRangeOfFaceTimeStamp: " + startGapIndex + " endIndexOfGap : " + endGapIndex + " dif : " + dif + " i : " + i + "Constante  wake up : " + Constant.WAKE_UP_VALUE + " nbr de result : " + Constant.NBR_OF_RESULT, 'c');
                    det = {
                        biggestGap: endGapIndex - startGapIndex,
                        startGapIndex: startGapIndex,
                        endGapIndex: endGapIndex,
                        duration: endGapIndex - startGapIndex,
                        durationMinute: ((arrData[endGapIndex].minuteIndex / 30) + 12).toFixed(2) - ((arrData[startGapIndex].minuteIndex / 30) + 12).toFixed(2),
                        minuteStartIndex: ((arrData[startGapIndex].minuteIndex / 30) + 12).toFixed(2),
                        minuteEndIndex: ((arrData[endGapIndex].minuteIndex / 30) + 12).toFixed(2)
                    };
                    // log.log("Det : " + JSON.stringify(det), 'c');
                    dif = endGapIndex - startGapIndex;
                }
                i = endGapIndex;
                // log.log("result startIndexOfGap: " + startGapIndex + " endIndexOfGap : " + endGapIndex + " dif : " + dif + " i : " + i, 'c');
            });
        }
        result(det);
    },


    asleepFallingTimeAlgorithm: function () {

    },

    getHourOfSleepAlgorithm: function () {

    },

    asleepDuringTimeAlgorithm: function (noisedata, arrNoisePik) {
        var asleepDuringTime = 0;
        var t = 0;
        var stop = false;
        var i = 0;
        var intervalInMinute = 20;

        return new Promise(function (resolve, error) {
            while (i <= arrNoisePik.length) {
                if (i + 1 < arrNoisePik.length && arrNoisePik[i + 1].minuteIndex - arrNoisePik[i].minuteIndex > intervalInMinute) {
                    asleepDuringTime += arrNoisePik[i + 1].minuteIndex - arrNoisePik[i].minuteIndex
                }
                i++;
            }
            if (asleepDuringTime === 0) {
                asleepDuringTime = 600 - arrNoisePik[arrNoisePik.length - 1].minuteIndex
            }
            resolve((asleepDuringTime / 30) * 60); //Return in minutes
        });
    }
};


