/**
 * Created by leo on 12/07/2017.
 */
var express = require('express');
var router = express.Router();
var Parse = require('parse/node');
var exports = require('../bin/www');
var request = require('request');
var fs = require('fs');
var Algo = require('../algorithm/algorithms');
var Tool = require('../tool/tools');
var HumanizeDuration = require('humanize-duration');
var Log = require('../tool/consoleLogs')
var Constant = require('../tool/constants');
const spawn = require('threads').spawn;


var currentNoiseLog;

var getDataFromFile = function (datafile) {
    var bytes = [];
    return new Promise(function (resolve, error) {
        request.get(datafile.url(), {encoding: null}, function (error, response, body) {
            // Log.log("response " + body.toString(), "l")
            if (!error) {
                var data = body;
                var responseData = Buffer.from(data.toString());
                for (var i = 0; i < data.length; i++) {
                    bytes.push(responseData[i] & 0xFF);
                }
                resolve(data);
            }
        });
    });
};

function getIndex(date) {
    let h = date.getUTCHours();
    let m = date.getUTCMinutes();

    if (h < 0)
        h = 0;
    if (h === 0) {
        return 360 + m / 2;
    }
    if (h >= 12) {
        h = h - 12;
    } else if (h < 12) {
        h = h + 12;
    }
    return h * 30 + m / 2;
}

function calculScore(sleepTimeIndex, wakeUpTimeIndex) {
    Log.log("calculScore sleepTimeIndex : " + sleepTimeIndex, "l");
    Log.log("calculScore wakeUpTimeIndex : " + wakeUpTimeIndex, "l");
    let nightParentDuration = 360;
    let score = 0;
    if (Constant.SLEEPING_TIME_FACE && Constant.WAKE_UP_TIME_FACE) {
        let indexSleepTimeFace = getIndex(Constant.SLEEPING_TIME_FACE);
        let indexWakeUpTimeFace = getIndex(Constant.WAKE_UP_TIME_FACE);
        Log.log("calculScore indexSleepTimeFace : " + Math.round(indexSleepTimeFace), "l");
        Log.log("calculScore indexWakeUpTimeFace : " + Math.round(indexWakeUpTimeFace), "l");
        let totalNightDurationForParent = indexWakeUpTimeFace - indexSleepTimeFace;
        let totalNightDurationForChild = wakeUpTimeIndex - sleepTimeIndex;
        score = (2 * 100 * totalNightDurationForChild / totalNightDurationForParent) - 100;
        Math.round(score);
        Log.log("calculScore scoref : " + score, "l");


        if (score < 0)
            score = 0;
        else if (score > 100 && score < 110)
            score = 100;
        else if (score > 110)
            score = 0;
        return score;
    } else
        return 0;
}

function saveInfo(details, sleepInfo, res) {

    var time = details.durationMinute > 24 ? details.durationMinute -= 24 : details.durationMinute;
    var timeSleep = details.minuteStartIndex > 24 ? details.minuteStartIndex -= 24 : details.minuteStartIndex;
    var timeUp = details.minuteEndIndex > 24 ? details.minuteEndIndex -= 24 : details.minuteEndIndex;
    var hourSleepDuration = (24 - Math.trunc(timeSleep)) + Math.trunc(timeUp);
    var REMI = Parse.Object.extend("Remi");
    var remiQuery = new Parse.Query(REMI);
    sleepInfo.set("bedTime", [Math.trunc(timeSleep), Math.round(60 * (timeSleep % 1))]);
    sleepInfo.set("wakeUpTime", [Math.trunc(timeUp), Math.round(60 * (timeUp % 1))]);
    sleepInfo.set("sleepDuration", [Math.trunc(time), Math.round(60 * (time % 1))]);
    sleepInfo.set("wakeUpFaceTime", Constant.WAKE_UP_TIME_FACE);
    sleepInfo.set("sleepFaceTime", Constant.SLEEPING_TIME_FACE);
    sleepInfo.set("remi", currentNoiseLog.get("remi"));
    sleepInfo.set("sleepIndex", details.startGapIndex);
    sleepInfo.set("wakeUpIndex", details.endGapIndex);
    let score = calculScore(details.startGapIndex, details.endGapIndex);
    sleepInfo.set("score", score);
    Log.log("score final: " + score, "l");
    if (currentNoiseLog.get("remi") !== null && currentNoiseLog.get("remi") !== undefined && currentNoiseLog.get("remi")) {
        remiQuery.get(currentNoiseLog.get("remi").id, {useMasterKey: true}, {
                success: function (remi) {
                    return remi;
                },
                error: function (object, error) {
                    Log.log("GettingREMIError : " + error.message, "e");
                    res.send("error");
                    return null;
                }
            }
        ).then(function (remi) {
            // Log.log("Inside the then with remi : " + remi.get("birthDate"), 's')
            sleepInfo.set("birthDay", remi.get("birthDate"));
            sleepInfo.save(null, { //save sleepInfo in Parse then save the current noiseLog with the new sleepInfo linked
                success: function (savedSleepInfo) {
                    // Log.log("sleepInfo saved. : " + savedSleepInfo.get("bedTime"), "s");
                    currentNoiseLog.set("sleepInfo", savedSleepInfo);
                    currentNoiseLog.save(null, {
                        success: function (obj) {
                            // Log.l og("noiselog saved. obj :" + obj.id, "s");
                            res.send("Done")
                        }, error: function (obk, error) {
                            res.send("error")
                        }
                    })
                },
                error: function (obk, error) {
                    Log.log("noiselog saved error : " + error.message, 'e');
                }
            });
        });
    } else
        res.send("error");
}

let getLogsData = function (obj, res) {
    return new Promise(function (resolve, error) {
        let NoiseLog = exports.Parse.Object.extend("noise_logs");
        let noiseLogQuery = new exports.Parse.Query(NoiseLog);
        Constant.USE_FACE_TIMESTAMP = false;
        noiseLogQuery.get(obj, {
            success: function (noiseLog) {
                currentNoiseLog = noiseLog;
                // Log.log("faceDataPsm : " + noiseLog.get('faceDataPsm'), 's');
                Constant.SLEEPING_TIME_FACE = null;
                Constant.WAKE_UP_TIME_FACE = null;
                if (currentNoiseLog.get('faceDataPsm') !== null && currentNoiseLog.get('faceDataPsm') && currentNoiseLog.get('faceDataPsm') !== undefined) {
                    var arr = currentNoiseLog.get('faceDataPsm').substring(0, currentNoiseLog.get('faceDataPsm').length - 1).split(" ");
                    for (var i = 0; i < arr.length; i++) {
                        var type = arr[i].replace(/.*:/, "");
                        var date = new Date(arr[i].replace(/:.*/, "") * 1000);
                        if (type === "2")
                            Constant.SLEEPING_TIME_FACE = date;
                        else if (type === "1")
                            Constant.WAKE_UP_TIME_FACE = date;
                        // Log.log("date : " + date.getHours() + " h  " + date.getMinutes(), 'e');
                    }
                }


                // console.log("noiselog " + currentNoiseLog.id + " : " + currentNoiseLog.get('timestamp'));
                getDataFromFile(currentNoiseLog.get('data')).then(noiseData => {
                    // Log.log("data" + currentNoiseLog.id + " : " + noiseData, 'e');
                    // Log.log("data size : " + noiseData.length, 'e');
                    // getDataFromFile(currentNoiseLog.get('lightData')).then(lightData => {
                    //     getDataFromFile(currentNoiseLog.get('tempData')).then(tempData => {
                    lightData = []
                    tempData = [];
                    getScore(noiseData, lightData, tempData, res).then(details => {
                        var SleepInfo = Parse.Object.extend("SleepInfo");
                        let sleepInfoQuery = new exports.Parse.Query(NoiseLog);
                        var sleepInfo;
                        // Log.log("get sleepinfo objectid from noislog : " + currentNoiseLog.get("sleepInfo").id, "l");
                        if (!currentNoiseLog.get("sleepInfo") || currentNoiseLog.get("sleepInfo") === undefined || currentNoiseLog.get("sleepInfo") === null) {
                            sleepInfo = new SleepInfo();
                            saveInfo(details, sleepInfo, res, noiseData);
                        } else {
                            currentNoiseLog.get("sleepInfo").fetch({
                                success: function (mSleepInfo) {
                                    // Log.log("get sleepinfo objectid from noislog : " + mSleepInfo.id, "l");
                                    saveInfo(details, mSleepInfo, res)

                                },
                                error: function (error) {
                                    Log.log("error during the fetch of the sleepinfo", "l");
                                    sleepInfo = new SleepInfo();
                                    saveInfo(details, sleepInfo, res);
                                }
                            });
                        }


                        //
                        // if (currentNoiseLog.get("sleepInfo") !== undefined) {
                        //
                        //     sleepInfoQuery.get(currentNoiseLog.get("sleepInfo").id, {
                        //         success: function (msleepInfo) {
                        //             sleepInfo = msleepInfo;
                        //             saveInfo(details, sleepInfo)
                        //         },
                        //         error: function (error) {
                        //             Log.log("creating a new sleepinfo error : " + error, "l");
                        //             sleepInfo = new SleepInfo();
                        //             saveInfo(details, sleepInfo);
                        //         }
                        //     })
                        // } else {
                        //     Log.log("creating a new sleepinfo", "l");
                        //     sleepInfo = new SleepInfo();
                        //     saveInfo(details, sleepInfo);
                        // }
                        // Log.log("\nsleepduration = " + Math.trunc(sleepDuration) + " hour and " + Math.round(60 * (sleepDuration % 1)) + " minutes ", "l");
                        // s = ("\nbedtime = " + Math.trunc(timeSleep) + " hour and " + Math.round(60 * (timeSleep % 1)) + " minutes ") + ("\nwakeup = " + Math.trunc(timeUp) + " hour and " + Math.round(60 * (timeUp % 1)) + " minutes");
                        // Log.log("biggestCurrentGap <= details.biggestGap = " + s, 's');
                    })
                });
                // });
                // });

            },
            error: function (object, error) {
                console.log("failed : " + error + " obj :" + object);
            }
        });
    });
};

var biggestCurrentGap = 0;
var currentDetails;
var tempDetails = undefined;

let reinitialize = function () {
    Constant.NBR_OF_RESULT = 2;
    currentDetails = {
        biggestGap: 0,
        startGapIndex: 0,
        endGapIndex: 0,
        minuteStartIndex: 0,
        minuteEndIndex: 0
    };
    isResultFound = false;
    tempDetails = undefined;
    biggestCurrentGap = 0;
};


let getScore = function (noiseData, lightData, tempData, res) {
    var arr = [20];
    // Log.log("Start get Score", 'z');
    return new Promise(function (resolve, error) {
        Tool.getDataArray(noiseData, lightData, 0, 720).then(arrData => { // retourne les données de bruit et de luminosité de 18h à 10h.
            Constant.USE_LIGHT_DATA = false;

            startAlgo(arrData, arr);
            if (tempDetails !== undefined && currentDetails !== undefined && tempDetails.biggestGap < currentDetails.biggestGap) {
                Log.log("tempDetails !== undefined", 'z');
                currentDetails = tempDetails;
            }
            Log.log("First StartAlgo RESULT = " + JSON.stringify(currentDetails), 'z');
            //
            if (currentDetails.endGapIndex === undefined || currentDetails.endGapIndex === 0 || isResultFound === false) {
                // Log.log("starting another startAlgo cause no correct result", 'z');
                arr = [15, 20, 25];
                // Constant.USE_LIGHT_DATA = true;
                startAlgo(arrData, arr);
                resolve(currentDetails);
            } else
                resolve(currentDetails);
            // Log.log("final result = " + currentDetails.endGapIndex, 'c');
        });
    });
};

var isResultFound = false;

let startAlgo = function (arrData, arr) {
    reinitialize();
    for (var a = 1; a <= 3; a++) {
        for (var i = 0; i < arr.length; i++) {
            Constant.WAKE_UP_VALUE = arr[i];
            // Log.log("constant test " + arrData.length, 's');
            Algo.getStartEndSleep(arrData, function (details) {
                // if (Object.keys(details).length === 0 || details.endGapIndex === undefined || details.endGapIndex === 713) {
                //     Log.log("No result found", 'z');
                //     return;
                // } else {
                    // Log.log("result found", 's');
                    // isResultFound = true;
                // }
                // Log.log("arrData[details.startGapIndex].minuteIndex : " + arrData[details.endGapIndex].minuteIndex, 'c');
                let startSleepTime = arrData[details.startGapIndex].minuteIndex / 30 * 60 * 60000;
                let endSleepTime = arrData[details.endGapIndex].minuteIndex / 30 * 60 * 60000;
                let sleepTime = details.dif / 30 * 60 * 60000;
                var date = new Date(endSleepTime);
                var hours = date.getHours();
                var minutes = date.getMinutes();
                if (details.startIndex >= 360)
                    hours += 13;
                if (details.endIndex < 360)
                    hours -= 13;
                var timeSleep = details.minuteStartIndex > 24 ? details.minuteStartIndex -= 24 : details.minuteStartIndex;
                var timeUp = details.minuteEndIndex > 24 ? details.minuteEndIndex -= 24 : details.minuteEndIndex
                // Log.log("Heure de couché = " + Math.trunc(timeSleep) + " hour " + Math.round(60 * (timeSleep % 1)) + " minutes", 's');
                // Log.log("Heure de levé = " + Math.trunc(timeUp) + " hour " + Math.round(60 * (timeUp % 1)) + " minutes", 's');
                //
                //
                // Log.log("details gap:" + details.biggestGap + " biggestCurrentGap : " + biggestCurrentGap, 'z');

                if (details.biggestGap > biggestCurrentGap && currentDetails.endGapIndex < details.endGapIndex && details.endGapIndex !== 720) {
                    // Log.log("save new details", 'z');
                    currentDetails = details;
                    biggestCurrentGap = details.biggestGap
                } else if (details.endGapIndex >= 721) {
                    tempDetails = details
                }
            });
        }
        Constant.NBR_OF_RESULT = a;
    }
};

router.post('/', function (req, res, next) {
    let obj = req.body.objectId;
    // const thread = spawn(function(input, done) {
    //     // Everything we do here will be run in parallel in another execution context.
    //     // Remember that this function will be executed in the thread's context,
    //     getLogsData(obj, res);
    //     Log.log("request score for : " + obj);
    //     done({ string : input.string, integer : parseInt(input.string) });
    // });

    // thread.send()
    getLogsData(obj, res);
    // res.send()
});

module.exports = router;
