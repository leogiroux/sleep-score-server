module.exports = {

    getDataArrayBetweenTimeStamp: function (start, end, data) {
        return new Promise(function (resolve, error) {
            var temp = [];
            for (var i = start, a = 0; i < end; i++, a++)
                temp[a] = data[i];
            resolve(temp)
        });
    },

    getDataArray: function (noiseLogs, lightLogs, start, end) {
        return new Promise(function (resolve, error) {
            var temp = [];

            let x = 180;
            temp.push({
                minuteIndex: 0,
                noiseValue: 255,
                lightValue: 11
            });

            for (var a = 1; a < x; a++) {
                temp.push({
                    minuteIndex: a,
                    noiseValue: 255,
                    lightValue: 11
                });
            }

            for (var i = start + a; i < end; i++) {
                temp.push({
                    minuteIndex: i,
                    noiseValue: noiseLogs[i],
                    lightValue: lightLogs[i]
                })
            }

            temp.push({
                minuteIndex: i,
                noiseValue: 0,
                lightValue: 11
            });

            resolve(temp)
        });
    },

    getRealHour: function (x) {

        x = x * 30 / 60 * 60000
        return x;

    }
};